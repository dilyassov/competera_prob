import os
import sys

from celery import Celery

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR.replace('\\', '/'))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'competera.settings')
app = Celery('competera')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
