# -*- coding: utf-8 -*-
import json
import math
import threading
import time
from random import random

from django.core.management.base import BaseCommand

from products.constants import REDIS_QUEUES_AMOUNT
from products.utils import Redis


def produce(conn, index):
    sku = '{0:06}'.format(int(random() * 100000)+1)
    min_price = 1000 + math.ceil(random() * 500)
    if min_price == 1500:
        max_price = min_price
    else:
        max_price = min_price + math.ceil(random() * (1500 - min_price))
    data = dict(sku=sku, min_price=min_price, max_price=max_price)

    conn.rpush('queue:%d' % index, json.dumps(data))


def payload(conn, index):
    while True:
        produce(conn, index)
        time.sleep(0.01)


class Command(BaseCommand):
    help = 'Generating payload'

    def handle(self, *args, **options):
        threads = REDIS_QUEUES_AMOUNT
        jobs = []
        conn = Redis().get_conn()
        for i in range(1, threads+1):
            thread = threading.Thread(target=payload, args=(conn, i))
            jobs.append(thread)

        for j in jobs:
            j.start()

        for j in jobs:
            j.join()
