# -*- coding: utf-8 -*-
import math
from random import random

from django.core.management.base import BaseCommand

from products.models import Product


class Command(BaseCommand):
    help = 'Generating objects'

    def get_price(self):
        return 1000 + math.ceil(random() * 500)

    def get_sku(self, i):
        return '{0:06}'.format(i)

    def handle(self, *args, **options):
        Product.objects.all().delete()
        for i in range(1, 100001):
            Product.objects.create(
                sku=self.get_sku(i),
                price=self.get_price()
            )
