# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from products.utils import CeleryTasks


class Command(BaseCommand):
    help = 'Reading payload'

    def handle(self, *args, **options):
        td = CeleryTasks()
        td.start()
