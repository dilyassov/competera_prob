import json

from django.utils import timezone

from .celery_app import app
from products.models import Product


@app.task(bind=False)
def update_values(queue):
    from products.utils import Redis
    conn = Redis().get_conn()
    queues = ['queue:%d' % queue]
    while True:
        packed = conn.blpop(queues, 1)
        if not packed:
            continue
        data = json.loads(packed[1])
        try:
            price = (data['min_price'] + data['max_price']) / 2
            Product.objects.filter(sku=data['sku']).update(price=price, datetime_updated=timezone.now())
        except Exception as e:
            pass
