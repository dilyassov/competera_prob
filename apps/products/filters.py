from datetime import timedelta

from django.utils import timezone
from rest_framework import filters
from products.constants import DEFAULT_LAST_UPDATE_MINUTES


class ProductMinutesFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        minutes = request.GET.get('minutes', DEFAULT_LAST_UPDATE_MINUTES)
        updated_from = timezone.now() - timedelta(minutes=int(minutes))
        return queryset.filter(datetime_updated__gte=updated_from)
