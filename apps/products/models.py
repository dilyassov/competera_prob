from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import gettext as _


class Product(models.Model):
    sku = models.CharField(_('SKU'), max_length=8, unique=True)
    price = models.IntegerField(_('Price'), validators=[MinValueValidator(1000), MaxValueValidator(1500)])
    datetime_created = models.DateTimeField(_('Datetime created'), auto_now_add=True)
    datetime_updated = models.DateTimeField(_('Datetime updated'), auto_now=True)

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
