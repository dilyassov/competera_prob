from celery import Celery


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class CeleryApp(object):
    __metaclass__ = Singleton

    def __init__(self):
        self.app = Celery('competera')
        self.app.config_from_object('django.conf:settings', namespace='CELERY')
        self.app.autodiscover_tasks()


app = CeleryApp().app
