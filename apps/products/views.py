from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination

from apps.products.filters import ProductMinutesFilter
from apps.products.serializers import ProductSerializer
from products.models import Product


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = ProductSerializer
    filter_backends = (ProductMinutesFilter,)
    queryset = Product.objects.all()
    pagination_class = StandardResultsSetPagination
    page_size = 20
