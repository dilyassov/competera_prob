from rest_framework.routers import DefaultRouter

from apps.products.views import ProductViewSet

router = DefaultRouter()
router.register(r'product', ProductViewSet, base_name='product')

urlpatterns = router.urls