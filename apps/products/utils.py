import redis
from django.conf import settings
from products.celery_app import app
from products.constants import REDIS_QUEUES_AMOUNT
from products.tasks import update_values


class Redis:
    def __init__(self, **kwargs):
        self.host = kwargs.get('host') or settings.REDIS_DATA_HOST
        self.port = kwargs.get('port') or settings.REDIS_DATA_PORT
        self.db = kwargs.get('db') or settings.REDIS_DATA_DB

    def get_conn(self):
        return redis.Redis(host=self.host, port=self.port, db=self.db)


class CeleryTasks:
    def __init__(self):
        self.amount = REDIS_QUEUES_AMOUNT

    def start(self):
        self.stop()
        for i in range(self.amount):
            update_values.delay(i+i)

    def stop(self):
        app.control.purge()
